# README #

This is a public repository for images used in URI Lab's READMEs and other documentation.


## Uploading

1. Login to bitbucket with write access to this repository.
2. Go to https://bitbucket.org/uri-labs/public-images/downloads/ 
3. Click 'Add files'

## Add Image to Markdown file

Markdown code for images


			![alt test](https://bitbucket.org/uri-labs/public-images/downloads/image-file-name.png)
